#!/bin/bash

for file in js/*.json
do
  aws ssm put-parameter --cli-input-json file://$file --overwrite
done